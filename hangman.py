import smartpy as sp

class Hangman(sp.Contract):
    def __init__(self, admin):
        self.init(
            admin = admin,
            paused = False,
            games = sp.big_map(tkey = sp.TAddress),
            metadata = {},
        )

    @sp.entry_point
    def build_game(self, params):
        sp.verify(~ self.data.paused)
        sp.verify(~ self.data.games.contains(sp.sender))
        sp.verify(~ (sp.len(params.word) == 0))
    
        letters = sp.map(l={}, tkey=sp.TNat, tvalue=sp.TString)
        with sp.for_("i", sp.range(0, sp.len(params.word), step=1)) as i:
            letters[i] = "_"

        self.data.games[sp.sender] = sp.record(
            player = sp.sender,
            word = params.word,
            guesses = sp.set(),
            guesses_number = sp.nat(0),
            status = letters,
            win = False,
        )

    @sp.entry_point
    def delete_game(self, params):
        sp.verify(~ self.data.paused)
        sp.verify(self.data.games.contains(sp.sender))

        del self.data.games[sp.sender]

    @sp.entry_point
    def setPause(self, params):
        sp.verify(sp.sender == self.data.admin)
        self.data.paused = params

    @sp.entry_point
    def guess(self, params):
        sp.verify(~ self.data.paused)
        sp.verify(self.data.games.contains(sp.sender))
        sp.verify((~ self.data.games[sp.sender].win) | (self.data.games[sp.sender].guesses_number == 12))
        sp.verify(sp.len(params.letter) == 1)

        game = self.data.games[sp.sender]

        sp.verify((~ game.guesses.contains(params.letter)))
        sp.verify((~ game.win))

        game.guesses.add(params.letter)
        game.guesses_number += 1

        with sp.if_(game.word.contains(params.letter)):
            with sp.for_("i", sp.range(0, sp.len(game.word), step=1)) as i:
                with sp.if_(game.word[i] == params.letter):
                    game.status[i] = params.letter

        print(game.status.values())
        
        with sp.if_((~ game.status.values().contains("_"))):
            game.win = True


        self.data.games[sp.sender] = game

sp.add_compilation_target("Hangman Compiled", Hangman(sp.test_account("admin")))